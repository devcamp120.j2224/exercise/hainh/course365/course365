var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 14,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        }
    ]
}   
    var gIsPopular ;
    var gIsTrending ;
    
    $(document).ready(function(){
        
        function isPopular(paramCourse){
            var vResul = []
            for ( var i = 0 ; i < paramCourse.courses.length ; i++){
                if (paramCourse.courses[i].isPopular === true){   
                    vResul.push(paramCourse.courses[i]) ;   
                }
            }    
            console.log(vResul) ;
            return vResul ;
        }

        function isTrending(paramCourse){
            var vResul = []
            for ( var i = 0 ; i < paramCourse.courses.length ; i++){
                if (paramCourse.courses[i].isTrending === true){   
                    vResul.push(paramCourse.courses[i]) ; 
                }
            }    
            console.log(vResul);
            return vResul ;
        }

        onPageLoading() ;

        function onPageLoading(){

            gIsPopular = isPopular(gCoursesDB);
            gIsTrending = isTrending(gCoursesDB);

            ShowData(gIsPopular , ".popular")
            ShowData(gIsTrending , ".trending")
        } 
          
        function ShowData(paramData,paramData1){
            var vResult;
            for(let i= 0; i< paramData.length ; i++){
                vResult = 
                `
                <div class="col-3">
                    <div class="card" >
                      <img class="card-img-top" id="img-subject" src="`+ paramData[i].coverImage +`" alt="Card image cap">
                      <div class="card-body">
                        <a href="">CSS:`+ paramData[i].courseName +`</a><br><br>
                        <i class="far fa-clock"> </i>&emsp;`+ paramData[i].duration +` <br><br>
                        <p><b>`+ paramData[i].price +"$"+` &emsp;</b> <del>`+ paramData[i].discountPrice +"$"+`</del> </p> 
                      </div>
                      <div class="card-footer">
                        <img style="width:40px" class="rounded-circle" src="`+ paramData[i].teacherPhoto + `" alt="">&emsp;`+ paramData[i].teacherName +`<i class = "far fa-bookmark float-right"> </i>
                      </div>
                    </div>
                  </div>
                  `
              $(paramData1).append(vResult);
            }
        }
        
        $(".all-course").on("click" , function(){
            window.location.href = "listCourse.html"
        })
    })